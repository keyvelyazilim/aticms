<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Carbon\Carbon;

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'yonetim'],function(){
    Route::get('/','AdminController@index')->name('admin.index');
    //Route::resource('ayarlar','AyarController');
    Route::get('/ayarlar','AyarController@index')->name('admin.index');
//    Route::post('ayar-guncelle','AdminController@ayarguncelle')->name('ayar.guncelle'); yanlış controller
    //Route::post('ayar-guncelle','AyarController@ayarguncelle')->name('ayar.guncelle');
    Route::post('ayar-guncelle/{id}','AyarController@ayarguncelle')->name('ayar.guncelle');
    Route::get('deneme-form','AyarController@denemeformu')->name('deneme.formu');
    Route::post('deneme','AyarController@deneme')->name('deneme.kaydet');
});

Route::get('/tarih', function () {
    return Carbon::now();
});


