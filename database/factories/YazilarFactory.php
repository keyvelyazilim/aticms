<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Yazi::class, function (Faker $faker) {
    $baslik = $faker->sentence;
    return [
        'baslik'=> $baslik,

        'icerik'=> $faker->text,

        'kategori'=> $faker->randomDigit,

        'user_id' => function () {
        return User::all()->random();
    },

        'resim'=>$faker->imageUrl($width = 200, $height = 200)

    ];
});
