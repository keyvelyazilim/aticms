<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableYazi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yazilar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('baslik');
            $table->text('icerik');
            $table->integer('kategori')->unsigned(); //aşağıda tabloları bağladığımız için kategori ve users içerisinde değer
            $table->integer('user_id')->unsigned(); // arayacaktır bu yüzden unsigned yani içerisinde herhangi bir değer yok
            $table->string('resim')->nullable(); //boş kalabilir

// users tablosu ile eşleştir on users,
// herhangi bir silme işlemi olursa ondelete cascade yani olduğu gibi davran farklı işlem yapma!

            $table->foreign('user_id')->references('id')->on('users')->ondelete('cascade');

//yazilarda ki kategori ile kategoriler tablosunda ki id ile yazilar tablosu içerisinde ki kategori ile eşleştirmek için

           // $table->foreign('kategori')->references('id')->on('kategoriler')->ondelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yazilar');
    }
}
