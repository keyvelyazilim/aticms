<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>AtiCMS - Yönetim Paneli v1.0</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin') }}/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin') }}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin') }}/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin') }}/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin') }}/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin') }}/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin') }}/assets/css/layout.css" rel="stylesheet" type="text/css">

    @yield('css')
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{ asset('admin') }}/global_assets/js/main/jquery.min.js"></script>
    <script src="{{ asset('admin') }}/global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('admin') }}/global_assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    @yield('js')
    <script src="{{ asset('admin') }}/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
    {{--<script src="{{ asset('admin') }}/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>--}}
    <script src="{{ asset('admin') }}/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
    {{--<script src="{{ asset('admin') }}/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>--}}
    {{--<script src="{{ asset('admin') }}/global_assets/js/plugins/ui/moment/moment.min.js"></script>--}}
    <script src="{{ asset('admin') }}/global_assets/js/plugins/pickers/daterangepicker.js"></script>

    <script src="{{ asset('admin') }}/assets/js/app.js"></script>
    <script src="{{ asset('admin') }}/global_assets/js/demo_pages/dashboard_boxed.js"></script>

    <!-- /theme JS files -->

</head>

<body class="layout-boxed-bg">

    <!-- Boxed layout wrapper -->
    <div class="d-flex flex-column flex-1 layout-boxed">

        <!-- Main navbar -->
        <div class="navbar navbar-expand-md navbar-dark">
            {{--TODO: pt-0 ve h-100 incelenecek logo boyutları için--}}
            <div class="navbar-brand">
                <a href="index.html" class="d-inline-block">
                    <img src="{{ asset('admin') }}/global_assets/images/logo_light.png" alt="">
                </a>
            </div>

            <div class="d-md-none">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                    <i class="icon-tree5"></i>
                </button>
                <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                    <i class="icon-paragraph-justify3"></i>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="navbar-nav ml-md-auto">
                    <li class="nav-item dropdown dropdown-user">
                        <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                            <img src="https://placeimg.com/50/50/tech" class="rounded-circle" alt="">
                            <span>Atilla</span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Profil Ayarları</a>
                            <a href="#" class="dropdown-item"><i class="icon-switch2"></i> Çıkış Yap</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

                <!-- Sidebar mobile toggler -->
                <div class="sidebar-mobile-toggler text-center">
                    <a href="#" class="sidebar-mobile-main-toggle">
                        <i class="icon-arrow-left8"></i>
                    </a>
                    Menü Kontrolü
                    <a href="#" class="sidebar-mobile-expand">
                        <i class="icon-screen-full"></i>
                        <i class="icon-screen-normal"></i>
                    </a>
                </div>
                <!-- /sidebar mobile toggler -->


                <!-- Sidebar content -->
                <div class="sidebar-content">

                    <!-- User menu -->
                    <div class="sidebar-user">
                        <div class="card-body">
                            <div class="media">
                                <div class="mr-3">
                                    <a href="#"><img src="https://placeimg.com/50/50/tech" width="38" height="38" class="rounded-circle" alt=""></a>
                                </div>

                                <div class="media-body">
                                    <div class="media-title font-weight-semibold">Atilla OKTAY</div>
                                    <div class="font-size-xs opacity-50">
                                        <i class="icon-pin font-size-sm"></i> &nbsp;İstanbul, TR
                                    </div>
                                </div>

                                <div class="ml-3 align-self-center">
                                    <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /user menu -->


                    <!-- Main navigation -->
                    <div class="card card-sidebar-mobile">
                        <ul class="nav nav-sidebar" data-nav-type="accordion">

                            <!-- Main -->
                            <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Menü</div> <i class="icon-menu" title="Main"></i></li>
                            <li class="nav-item">
                                <a href="index.html" class="nav-link active">
                                    <i class="icon-home4"></i>
                                    <span>
                                            Yönetim Paneli
                                        </span>
                                </a>
                            </li>
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-color-sampler"></i> <span>Kategori İşlemleri</span></a>

                                <ul class="nav nav-group-sub" data-submenu-title="Categories">
                                    <li class="nav-item"><a href="#" class="nav-link">Tüm Kategoriler</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link">Yeni Kategori Ekle</a></li>
                                </ul>
                            </li>
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-stack"></i> <span>Sayfa İşlemleri</span></a>

                                <ul class="nav nav-group-sub" data-submenu-title="Pages">
                                    <li class="nav-item"><a href="#" class="nav-link">Tüm Sayfalar</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link">Yeni Sayfa Ekle</a></li>
                                </ul>
                            </li>
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-list2"></i> <span>Site Menü İşlemleri</span></a>

                                <ul class="nav nav-group-sub" data-submenu-title="Categories">
                                    <li class="nav-item"><a href="#" class="nav-link">Menüyü Görüntüle</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link">Menüyü Düzenle</a></li>
                                </ul>
                            </li>
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-calendar"></i> <span>Etkinlik Programı</span></a>

                                <ul class="nav nav-group-sub" data-submenu-title="Categories">
                                    <li class="nav-item"><a href="#" class="nav-link">Tüm Etkinlikler</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link">Yeni Etkinlik Ekle</a></li>
                                </ul>
                            </li>
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-people"></i> <span>Kullanıcı İşlemleri</span></a>

                                <ul class="nav nav-group-sub" data-submenu-title="Users">
                                    <li class="nav-item nav-item-submenu">
                                        <a href="#" class="nav-link">Üye İşlemleri</a>
                                        <ul class="nav nav-group-sub">
                                            <li class="nav-item"><a href="#" class="nav-link">Tüm Üyeler</a></li>
                                            <li class="nav-item"><a href="#" class="nav-link">Yeni Üye Ekle</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item nav-item-submenu">
                                        <a href="#" class="nav-link">Editör İşlemleri</a>
                                        <ul class="nav nav-group-sub">
                                            <li class="nav-item"><a href="#" class="nav-link">Tüm Editörler</a></li>
                                            <li class="nav-item"><a href="#" class="nav-link">Yeni Editör Ekle</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item nav-item-submenu">
                                        <a href="#" class="nav-link">Yönetici İşlemleri</a>
                                        <ul class="nav nav-group-sub">
                                            <li class="nav-item"><a href="#" class="nav-link">Tüm Yöneticiler</a></li>
                                            <li class="nav-item"><a href="#" class="nav-link">Yeni Yönetici Ekle</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item-divider"></li>
                                    <li class="nav-item"><a href="#" class="nav-link">Üye Ayarları</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link">Editör Ayarları</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link">Yönetici Ayarları</a></li>
                                </ul>
                            </li>
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-wrench3"></i> <span>Site Ayarları</span></a>

                                <ul class="nav nav-group-sub" data-submenu-title="Settings">
                                    <li class="nav-item"><a href="#" class="nav-link">Genel Ayarlar</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link">Kategori Ayarları</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link">Sayfa Ayarları</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link">Google Ayarları</a></li>
                                    <li class="nav-item"><a href="#" class="nav-link">Sosyal Medya Ayarları</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-list-unordered"></i>
                                    <span>Yenilikler</span>
                                    <span class="badge bg-blue-400 align-self-center ml-auto">1.0</span>
                                </a>
                            </li>
                            <!-- /main -->
                        </ul>
                    </div>
                    <!-- /main navigation -->

                </div>
                <!-- /sidebar content -->

            </div>
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-light">
                    <div class="page-header-content header-elements-md-inline">
                        <div class="page-title d-flex">
                            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Yönetim Paneli</span> - Ana Sayfa</h4>
                        </div>
                    </div>

                    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                        <div class="d-flex">
                            <div class="breadcrumb">
                                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Yönetim Paneli</a>
                                <span class="breadcrumb-item active">Ana Sayfa</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">
                    <!-- hataların görünmesini istediğin yere dahil ediyoruz-->
                    @include('admin.hatalar')
                    @yield('content')
                </div>
                <!-- /content area -->


                <!-- Footer -->
                <div class="navbar navbar-expand-lg navbar-light">
                    <div class="text-center d-lg-none w-100">
                        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                            <i class="icon-unfold mr-2"></i>
                            Alt Bilgi
                        </button>
                    </div>

                    <div class="navbar-collapse collapse" id="navbar-footer">
                            <span class="navbar-text">
                                &copy; 2019. BKİ - Yönetim Paneli <a href="https://keyvel.com" target="_blank">Keyvel - Yazılım Teknolojileri</a> tarafından programlanmıştır.
                            </span>

                        <ul class="navbar-nav ml-lg-auto">
                            <li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> Teknik Destek</a></li>
                            <li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Dokümantasyon</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /footer -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->
    </div>
    @include('sweetalert::alert')
</body>

</html>
