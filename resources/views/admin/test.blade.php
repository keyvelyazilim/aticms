@extends ('admin.master')

@section('css')

@endsection


@section('js')
    <script src="{{ asset('admin') }}/global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="{{ asset('admin') }}/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="{{ asset('admin') }}/global_assets/js/demo_pages/form_layouts.js"></script>
@endsection

@section('content')
    <!-- Basic layout-->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Genel Ayarlar</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <!-- kopyala yapıştır yapmışssın form içerisinde sarı renk gitti gördüğün gibi -->

            <form action="{{route('deneme.kaydet')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="ayar_id" value="1">
                <div class="form-group">
                    <label>dizi:</label>
                    <input type="text" class="form-control" name="isimler[]" value="[ali,ahmet,ayşe,osman,cuma]">
                </div>


                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Güncelle <i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- /basic layout -->
@endsection
