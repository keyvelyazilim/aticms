@if ($errors->any())


    @foreach ($errors->all() as $error)

        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Hata</strong> {{$error}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>

    @endforeach



@endif


<!-- bu bölüm validatordan gelen hataları ekrana bastırmak için bunu istediğin gibi şekillendirebilirsin-->
