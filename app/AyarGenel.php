<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AyarGenel extends Model
{
    protected $table = 'ayarlar';
    protected $guarded = [];
}
