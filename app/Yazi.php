<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Yazi extends Model
{
    protected $table ='yazilar';
    protected $guarded =[]; //yazılar tablosu içerisinde ki tüm alanların doldurabilir olduğunu bildirmek

    public function user()  {
        return $this->belongsTo('App\User','id', 'user_id');

    }

}


