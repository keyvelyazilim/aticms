<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detay extends Model
{
    protected $table ='detaylar'
    protected $guarded = [];


    public function user()    { //detaylar tablosundan kullanıcıya erişim için yaptık

        return $this->belongsTo('App\User','id','user_id');

    }
}
