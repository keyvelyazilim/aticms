<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function yazilari()     {

    return $this->hasMany('App\Yazi','user_id','id');


    }


    public function detaylari()    {   //bir adet detay sutünü olacak her bir kullanıcı için

        return $this->hasOne('App\Detay','user_id','id');
        
    }



}
