<?php

namespace App\Http\Controllers;

use App\AyarGenel;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AyarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ayarlar = AyarGenel::find(1)->first();
        return view('admin.ayarlar.genel', compact('ayarlar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ayarguncelle(Request $request , $id)
    {
       /* dd($request->all());*/
       /* $this->validate(request(), array(
            'baslik' => 'required',
            'aciklama' => 'required',
        ));*/
       //bu validator yöntemini kullan
       $validator = Validator::make($request->all(),[
           'baslik'=>'required|string|max:255', //gördünmü gelen ile formdaki name farklı
           'aciklama'=>'required',// name değerleri aynı olmalı.

       ]);
       if($validator->fails()){
       return redirect()->back()->withErrors($validator)->withInput();
       };






        //hata yok ama başarılı ekrana yazmıyor. Çünkü uyarı eklentisi çalışmıyor.
        //paketleri güncellesin
        //altdaki Sadri Hoca'nın 80. satırdaki kodu hata veriyor. :((
//        $ayar = AyarGenel::find(Request->ayar_id);//gizli inputtan gelen ayar_id değeri database içerisindeki ayar değeri oluyor.
        //diğer yöntem ise hiç id ile uğraşmadan direk kendin idyi verebilirsin aşağıdaki gibi
         $ayar = AyarGenel::find($request->id);//ister direk 1 yazarsın istersen formda verdiğimiz değeri.

         $ayar->baslik = request('baslik');
        $ayar->aciklama = request('aciklama');
        $ayar->email = request('mail');

        if (request()->hasFile('image')) {
            $this->validate(request(), array('image' => 'image|mimes:png,jpg,jpeg,gif|max:2048'));
            $resim = request()->file('image');
            $dosya_adi = 'logo' . '-' . time() . '.' . $resim->extension();
            if ($resim->isValid()) {
            $hedef_klasor = 'uploads/dosyalar';
            $dosya_yolu = $hedef_klasor . '/' . $dosya_adi;
            $resim->move($hedef_klasor, $dosya_adi);
            $ayar->logo = $dosya_yolu;
        }
        }
        $ayar->save();

        if ($ayar) {
            Alert::success('Başarılı', 'Success Message');
            return back();
        } else {
            Alert::error('Hata', 'Success Message');
            return back();
        }
    }

    //şuan için sorun görünmüyor.:)

    //hocam çok teşekkürler ben değişken isimlerini karşılarken özellikle farklı kullandım.

    //key value olayını yani hangisi anahtar nereyi etkiliyor onu anlamak bir yerde ing

    // diğer yerde tr yazdım form da olsun controller da olsun ama karşıklığa sebep olmuşum sanırım

    // hepsi aynı isimde olunca bu sefer mantığı kavramak açısından ezbercilik gibi oluyor :)

    // herşey için çok ama çok teşekkürler değerli vaktinizi ayırdınız sağolun hocam.

    //rica ederim. Formdan gelen verinin ismi ile controllerdaki isim aynı olmalı. aksi halde hata verir yada kaydetmez.

    //key value olayı sadece foreach döngülerinde var.

    //isimler isminde biz dizi gelsin formdan

    public function denemeformu(){

     return view('admin.test');

    }



    public function deneme(Request $request)
    {
//virgül ilke ayrılmıış yada array iiçerisinde dizilmiş olan elemanları parçalamak ve her bir elemanı almak için
        //foreach kullanıyoruz. burada key value olayı oluyor.
       foreach($request->isimler as $anahtar => $deger){


       echo '<br>'.$anahtar.' - '.$deger.'<br>';
//demeke istediğimi şimdi net anlayacaksın


           // 0 ibaresi dizinin içerisindeki elemanının sıra numarasını verir.
           // dolayısıyla bu yüzden foreach içerisindeki bir elamanınn sıra numarasını öğrenmek için anahtar yani $key => $value kulalnıyourz

           //aunmarım anlatabilmişimdir:D  çok net anlaşıldı hocam.

           //tamamdır zaman şimdi çalışmalara devam:)

           //çok teşekkürler hocam. sağolun. :) görüşmek üzre;)

       }

    }


    /**
     * Remove the specified resource from storage.
     * :D
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

 /*   public function guncelle(Request $request,$id){

     dd($request->all());


    }*/
}
